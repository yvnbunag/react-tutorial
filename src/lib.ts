import { useState } from 'react'

export interface ToggleMachine<Values extends Array<unknown>> {
  current: Values[number],
  toggle: ()=> void,
  override: (value: Values[number])=> void
  reset: () => void,
}

type ToggleMachineResponse<
  Values extends Array<unknown>
> = ToggleMachine<Values> extends {
    current: infer Current,
    toggle: infer Toggle,
    override: infer Override,
    reset: infer Reset,
  }
    ? [Current, Toggle, Override, Reset]
    : never

export function useToggleMachine<
  Values extends Array<unknown>
>(values: Values): ToggleMachineResponse<Values> {
  const [index, setIndex] = useState(0)
  const reset = () => setIndex(0)

  return [
    values[index],
    () => {
      const nextIndex = index + 1

      if (nextIndex < values.length) return setIndex(nextIndex)

      return reset()
    },
    (override) => {
      const overrideIndex = values.findIndex((value) => value === override)

      setIndex(overrideIndex)
    },
    reset
  ]
}

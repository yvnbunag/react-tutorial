import type { Entry } from './types'

interface SquareProps {
  className: string,
  value: Entry
  onClick: () => void
}

export function Square(props: SquareProps) {
  return (
    <button className={`square ${props.className}`} onClick={props.onClick}>
      {props.value}
    </button>
  )
}

export enum players {
  X ='X',
  O ='O'
}

export type Player = players

export type Entry = Player | null

export type Row = [Entry, Entry, Entry]

export type Matrix = [...Row, ...Row, ...Row]

export type MatrixKey = keyof Matrix

export type History = Array<{
  matrix: Matrix,
  player: Player,
  position?: MatrixKey,
}>

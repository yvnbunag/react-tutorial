import { Square } from './Square'

import type { Matrix, MatrixKey } from './types'

interface BoardProps {
  squares: Matrix,
  onClick: (index: number)=> void
  boundary?: number,
  highlighted?: Array<MatrixKey>
}

export function Board(props: BoardProps) {
  const {  boundary = 3, highlighted = []} = props
  const boundaryKeys = Array.from(Array(boundary).keys())
  const board = boundaryKeys.map((rowPosition, rowIndex) => {
    const rows = boundaryKeys.map((columnPosition) => {
      const index = columnPosition
        + (rowPosition)
        + (rowIndex * (boundaryKeys.length - 1))

      return <Square
        className={highlighted.includes(index) ? 'highlighted' : ''}
        key={columnPosition}
        value={props.squares[index]}
        onClick={() => props.onClick(index)}
      />
    })

    return <div key={rowPosition} className="board-row">{rows}</div>
  })

  return <div>{board}</div>
}

import React, { useState } from 'react'

import { players } from './types'
import { Board } from './Board'
import { useToggleMachine } from './lib'
import './index.css'

import type { Player, Matrix, MatrixKey, History } from './types'
import type { ToggleMachine } from './lib'

type Step = number

type WinningCombination = [MatrixKey, MatrixKey, MatrixKey]

interface Winner {
  player: Player,
  combination: WinningCombination,
}

const tutorialURL = 'https://reactjs.org/tutorial/tutorial.html'
const playerSequence = [players.X, players.O]
const initial = {
  board: Array(9).fill(null) as Matrix,
  step: 0,
}
const winningCombinations: Array<WinningCombination> = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
]
const matrixMap = [
  [0, 0],
  [0, 1],
  [0, 2],
  [1, 0],
  [1, 1],
  [1, 2],
  [2, 0],
  [2, 1],
  [2, 2],
]

function calculateWinner(board: Matrix): Winner | null {
  for (const combination of winningCombinations) {
    const [first, second, third] = combination

    if (
      board[first]
      && board[first] === board[second]
      && board[second] === board[third]
    ) return { player: board[first] as Player, combination }
  }

  return null
}

function createMark(
  history: History,
  step: Step,
  player: Player,
  setHistory: React.Dispatch<React.SetStateAction<History>>,
  setStep: React.Dispatch<React.SetStateAction<Step>>,
  togglePlayer: () => void,
) {
  return (position: number) => {
    const currentHistory = history[step]

    setHistory((history) => history.slice(0, step + 1))

    if (currentHistory.matrix[position]) return

    setStep((step) => step + 1)
    setHistory((history) => {
      const currentHistory = history[history.length - 1]
      const matrix = [
        ...currentHistory.matrix.slice(0, position),
        player,
        ...currentHistory.matrix.slice(position + 1)
      ] as Matrix
      const next: History[number] = {
        player,
        matrix,
        position
      }

      return [...history, next]
    })
    togglePlayer()
  }
}

function createTimeTravel(
  move: number,
  setStep: React.Dispatch<React.SetStateAction<Step>>,
  setPlayer: ToggleMachine<typeof playerSequence>['override'],
) {
  return () => {
    const isEven = move % 2

    setStep(move)
    setPlayer(playerSequence[isEven ? 1 : 0])
  }
}

export function Game() {
  const [
    player,
    togglePlayer,
    setPlayer,
    resetPlayer,
  ] = useToggleMachine(playerSequence)
  const [step, setStep] = useState<Step>(initial.step)
  const [history, setHistory] = useState<History>([{
    matrix: initial.board,
    player,
  }])
  const [isDescending, toggleIsDescending] = useToggleMachine([false, true])
  const currentHistory = history[step]
  const winner = calculateWinner(currentHistory.matrix)
  const status = (() => {
    if (winner) return `Winner: ${winner.player}`

    if (history.length - 1 === initial.board.length) return 'Draw'

    return `Next player: ${player}`
  })()
  const mark = winner
    ? () => {}
    : createMark(history, step, player, setHistory, setStep, togglePlayer)
  const restart = history.length - 1
    ? () => {
      resetPlayer()
      setStep(initial.step)
      setHistory((history) => history.slice(0, 1))
    }
    : () => {}
  const moveHistory = (() => {
    const computedHistory = history.map((currentHistory, move) => {
      const description = move
        ? (() => {
          const position = typeof currentHistory.position === 'number'
            ? `(${matrixMap[currentHistory.position]})`
            : null
          const description = `[${currentHistory.player} ${position}]`

          return `Go to move # ${move} ${description}`
        })()
        : `Go to game start`

      return (
        <li key={move}>
          <button
            className={step === move ? 'selected' : ''}
            onClick={createTimeTravel(move, setStep, setPlayer)}
          >
            {description}
          </button>
        </li>
      )
    })

    if (isDescending) return computedHistory.reverse()

    return computedHistory
  })()

  return (
    <>
      <div className="title">
        <h1>Tic-Tac-Toe</h1>
        <a href={tutorialURL}>{tutorialURL}</a>
      </div>
      <div className="game">
        <div className="game-board">
          <Board
            squares={currentHistory.matrix}
            highlighted={winner?.combination}
            onClick={mark} boundary={3}
          />
          <div className="game-control">
            <button className="button centered" onClick={restart}>Restart</button>
          </div>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <hr/>

          History
          <button className="button" onClick={toggleIsDescending}>
            {isDescending ? 'Descending' : 'Ascending'}
          </button>
          <ol reversed={isDescending}>{moveHistory}</ol>
        </div>
      </div>
    </>
  )
}
